const cars = (inventory) =>{
    const AudiAndBMW = [];
    for(let i=0;i< inventory.length;i++){
        if(inventory[i].car_make === 'BMW' || inventory[i].car_make === 'Audi'){
            AudiAndBMW.push(inventory[i]);
        }
    }
    return AudiAndBMW;
}
module.exports = cars;