const toSort = (inventory) =>{
    if(inventory){
    inventory.sort((a,b)=> {
        if(a.car_model>b.car_model){
            return 1;
        }if(b.car_model>a.car_model){
            return -1;
        }
        return 0;
    });
    return inventory;
}else{
    return "undefined";
}
};
module.exports = toSort;