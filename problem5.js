const olderCars = (inventory)=>{
    if(inventory){
    const years = require("./problem4");
    const yearsArray = years(inventory);
    const oldCars =[];
    for(let i=0;i< yearsArray.length;i++){
        if(yearsArray[i] < 2000){
            oldCars.push(yearsArray[i]);
        }
    }
    return oldCars;
}else{
    return "undefined";
}
}

module.exports = olderCars;